<?php
define("IN_MYBB", 1);
define('CN_ABPPURL', str_replace('.php', '', basename(__FILE__)));
require_once "./global.php";
require_once MYBB_ROOT . "inc/functions_post.php";
if ($mybb->input['plid'] != '') {
    $res = $db->simple_select(CN_ABPPURL, 'purl', "plink='" . $mybb->input['plid'] . "'");
    if ($db->num_rows($res) == 1) {
        $link = $db->fetch_field($res, 'purl');
    } else {
        $link = $mybb->settings['bburl'];
    }
} else {
    $link = $mybb->settings['bburl'];
}
header_remove();
echo <<<EOF
<html>
<head>
    <meta name="referrer" content="no-referrer" />
    <script>window.location.replace("$link");</script>
</head>
<body><p>Redirecting</p></body>
</html>
EOF;
