<?php
// General
$l['abppurl_name'] = 'ABP Protected URL';
$l['abppurl_desc'] = 'Replace protected url with an internal link';

// Settings
$l['abppurl_setting_name'] = 'ABP Protected URL settings';
$l['abppurl_setting_desc'] = 'Short settings for the protection';
$l['abppurl_forums'] = 'Forums';
$l['abppurl_forums_desc'] = 'Choose where the plugin is applicable';
