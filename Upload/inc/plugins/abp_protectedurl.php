<?php
/**
 * ABP Protected URL
 * Replace protected url with an internal link
 * Optionnaly remove the referrer
 * (c) CrazyCat 2022
 */
if (!defined("IN_MYBB"))
    die('Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.');

define('CN_ABPPURL', str_replace('.php', '', basename(__FILE__)));

/**
 * Displayed informations
 */
function abp_protectedurl_info() {
    global $lang;
    $lang->load(CN_ABPPURL);
    return ['name' => $lang->abppurl_name,
        'description' => $lang->abppurl_desc . '<a href=\'https://ko-fi.com/V7V7E5W8\' target=\'_blank\'><img height=\'30\' style=\'border:0px;height:30px;float:right;\' src=\'https://az743702.vo.msecnd.net/cdn/kofi1.png?v=0\' border=\'0\' alt=\'Buy Me a Coffee at ko-fi.com\' /></a>',
        'website' => 'https://gitlab.com/ab-plugins/abp-selfuploader',
        'author' => 'CrazyCat',
        'authorsite' => 'http://community.mybb.com/mods.php?action=profile&uid=6880',
        'version' => '1.0',
        'compatibility' => '18*',
        'codename' => CN_ABPPURL
    ];
}

function abp_protectedurl_install() {
    global $db, $mybb, $lang;
    $lang->load(CN_ABPPURL);
    $settinggroups = [
        'name' => CN_ABPPURL,
        'title' => $lang->abppurl_setting_name,
        'description' => $lang->abppurl_setting_desc,
        'disporder' => 0,
        'isdefault' => 0
    ];
    $db->insert_query('settinggroups', $settinggroups);
    $gid = $db->insert_id();
    $settings = [
        [
            'gid' => $gid,
            'name' => CN_ABPPURL . '_forums',
            'title' => $lang->abppurl_forums,
            'description' => $lang->abppurl_forums_desc,
            'optionscode' => 'forumselect',
            'value' => '-1',
            'disporder' => 1
        ]
    ];
    $db->insert_query_multiple('settings', $settings);
    $db->write_query("CREATE TABLE IF NOT EXISTS " . TABLE_PREFIX . CN_ABPPURL . " (purl VARCHAR(250) NOT NULL , plink VARCHAR(50) NOT NULL , PRIMARY KEY (purl))");
    rebuild_settings();
}

function abp_protectedurl_is_installed() {
    global $db;
    return $db->table_exists(CN_ABPPURL);
}

function abp_protectedurl_uninstall() {
    global $db;
    if (abp_protectedurl_is_installed()) {
        $db->write_query("DROP TABLE " . TABLE_PREFIX . CN_ABPPURL);
    }
    $db->delete_query('settings', "name LIKE '" . CN_ABPPURL . "%'");
    $db->delete_query('settinggroups', "name='" . CN_ABPPURL . "'");
    rebuild_settings();
}

function abp_protectedurl_activate() {
    
}

function abp_protectedurl_deactivate() {
    
}

$plugins->add_hook('datahandler_post_validate_post', 'abp_protectedurl_postcheck');
$plugins->add_hook('datahandler_post_validate_thread', 'abp_protectedurl_postcheck');

function abp_protectedurl_postcheck(&$datahandler) {
    global $mybb;
    if (!isset($datahandler->data['message'])) {
        return;
    }
    $datahandler->data['message'] = preg_replace_callback('/\[purl\](.+)\[\/purl\]/U', 'abp_protectedurl_replace', $datahandler->data['message']);
}

function abp_protectedurl_replace($matches) {
    global $mybb, $db;
    $already = $db->simple_select(CN_ABPPURL, 'plink', "LOWER(purl)='".$db->escape_string($matches[1])."'");
    if ($db->num_rows($already)>0) {
        $plink = $db->fetch_field($already, 'plink');
    } else {
        $plink = uniqid('', $true);
        $db->insert_query(CN_ABPPURL, ['purl' => $matches[1], 'plink' => $plink]);
    }
    $link = $mybb->settings['bburl'].'/'.CN_ABPPURL.'.php?plid='.$plink;
    return '[url='.$link.']'.$link.'[/url]';
}